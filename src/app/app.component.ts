import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  template: `
    <mat-toolbar color="primary" [routerLink]="['/']" >NAGSER.RU - Портфолио</mat-toolbar>
    <router-outlet></router-outlet>
  `,
  styles: [
    'mat-toolbar {cursor: pointer}'
  ]
})
export class AppComponent {
}
