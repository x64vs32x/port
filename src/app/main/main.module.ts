import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ListModule } from './components/list/list.module';
import { ItemModule } from './components/item/item.module';
import { HttpClientModule } from '@angular/common/http';
import { DataService } from './services/data.service';

@NgModule({
  imports: [
    CommonModule,
    ListModule,
    ItemModule,
    HttpClientModule
  ],
  providers: [
    DataService
  ],
  declarations: []
})
export class MainModule { }
