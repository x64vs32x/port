export interface PortItem {
  id: number;
  name: string;
  preview: string;
  tags: string;
  images: {
    name: string;
    src: string[]
  }[];
}
