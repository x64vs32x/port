import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { PortItem } from '../interfaces/port-item';

@Injectable()
export class DataService {

  constructor(private http: HttpClient) {
  }

  getData(): Observable<PortItem[]> {
    return this.http.get<PortItem[]>('assets/data.json');
  }

}
