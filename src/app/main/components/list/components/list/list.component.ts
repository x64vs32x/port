import { Component, OnInit } from '@angular/core';
import { DataService } from '../../../../services/data.service';
import { Observable } from 'rxjs/Observable';
import { PortItem } from '../../../../interfaces/port-item';

@Component({
  selector: 'app-list',
  template: `
    <div class="list-container">
      <app-card *ngFor="let item of data | async" [item]="item"></app-card>
    </div>
  `
})
export class ListComponent implements OnInit {

  data: Observable<PortItem[]>;

  constructor(private dataService: DataService) {
    this.data = this.dataService.getData();
  }

  ngOnInit() {
  }

}
