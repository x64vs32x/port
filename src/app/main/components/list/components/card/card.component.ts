import { Component, Input, OnInit } from '@angular/core';
import { PortItem } from '../../../../interfaces/port-item';

@Component({
  selector: 'app-card',
  template: `
    <mat-card>
      <mat-card-header>
        <div mat-card-avatar class="example-header-image"></div>
        <mat-card-title>{{item.name}}</mat-card-title>
        <mat-card-subtitle>{{item.tags}}</mat-card-subtitle>
      </mat-card-header>
      <img [routerLink]="['view', item.id]" mat-card-image [src]="item.preview" [alt]="item.name">
      <mat-card-actions>
        <a [routerLink]="['view', item.id]" mat-button color="primary">
          <mat-icon>keyboard_backspace</mat-icon>
          ПОДРОБНЕЕ</a>
      </mat-card-actions>
    </mat-card>
  `,
  styles: [
    'img {cursor: pointer}',
    'mat-icon {transform: scale(-1, 1)}'
  ]
})
export class CardComponent implements OnInit {

  @Input() item: PortItem;

  constructor() {
  }

  ngOnInit() {
  }

}
