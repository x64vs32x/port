import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { DataService } from '../../../../services/data.service';
import 'rxjs/add/operator/mergeMap';
import 'rxjs/add/operator/withLatestFrom';
import 'rxjs/add/operator/filter';
import { from } from 'rxjs/observable/from';
import { PortItem } from '../../../../interfaces/port-item';
import 'rxjs/add/operator/take';
import 'rxjs/add/operator/distinctUntilChanged';
import 'rxjs/add/operator/do';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-item',
  template: `
    <div class="list-container" *ngIf="item">
      <p><a [routerLink]="['/']" mat-button color="primary">
        <mat-icon>keyboard_backspace</mat-icon>
        Назад</a></p>
      <mat-card>
        <mat-card-title>{{item.name}}</mat-card-title>
        <img mat-card-image [src]="item.preview" [alt]="item.name">
      </mat-card>
      <mat-card *ngFor="let image of item.images">
        <mat-card-title>{{image.name}}</mat-card-title>
        <img mat-card-image *ngFor="let address of image.src" [src]="address" [alt]="image.name">
      </mat-card>
      <p><a [routerLink]="['/']" mat-button color="primary">
        <mat-icon>keyboard_backspace</mat-icon>
        Назад</a></p>
    </div>
  `
})
export class ItemComponent implements OnInit, OnDestroy {

  item: PortItem = null;
  subscriptions: Subscription[] = [];

  constructor(private route: ActivatedRoute, private dataService: DataService) {
    const s = this.route.params
      .flatMap(({id}) => {
        return this.dataService.getData()
          .flatMap(data => from(data))
          .filter(item => item.id === Number(id))
          .take(1);
    })
      .subscribe(item => this.item = item);
    this.subscriptions.push(s);
  }

  ngOnInit() {
  }

  ngOnDestroy() {
    this.subscriptions.forEach(s => s.unsubscribe());
  }

}
