import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';


import { AppComponent } from './app.component';
import { MatToolbarModule } from '@angular/material';
import { RouterModule, Routes } from '@angular/router';
import { MainModule } from './main/main.module';
import { ListComponent } from './main/components/list/components/list/list.component';
import { ItemComponent } from './main/components/item/components/item/item.component';

const appRoutes: Routes = [
  {path: '', component: ListComponent},
  {path: 'view/:id', component: ItemComponent},
];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    RouterModule.forRoot(
      appRoutes,
      {useHash: true}
    ),
    BrowserModule,
    MatToolbarModule,
    MainModule,
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
